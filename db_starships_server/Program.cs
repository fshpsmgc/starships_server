﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace db_starships_server {
    class Program {
        static void Main(string[] args) {
            Log.Init();
            if (!DBController.Exists()) {
                Log.Write("Database not found, creating...", "system");
                DBController.CreateDatabase();
            }

            Log.Write("Server starting", "system");
            CommandProcessor.Init();
            Server server = new Server(8888);
        }
    }
}
