﻿using System;
using System.Collections;

public class hulls {
    public string name;
    public string sprite_name;
    public int max_hp;
    public int weapon_mount_count;
    public int max_crew_count;
    public int max_fuel;
    public int defence;
    public int weight;
}

public class command_units {
    public string name;
    public string sprite_name;
    public int max_energy;
    public int evasion;
    public int weight; 
}

public class engines {
    public string name;
    public string sprite_name;
    public int range;
    public int speed;
    public int weight;
}

public class items {
    public string name;
    public string sprite_name;
    public int sell_cost;
    public string description;
}

public class weapons {
    public string name;
    public string sprite_name;
    public int damage;
    public int accuracy;
    public int fire_rate;
}

public class ships {
    public string name;
    public int hull_id;
    public int engine_id;
    public int cu_id;
}

public class missions_enemies {
    public int mission_id;
    public int ship_id;
}

public class missions_rewards {
    public int mission_id;
    public int item_id;
}

public class missions {
    public string name;
    public string description;
    public int money_reward;
    public int length;
    public int distance;
}