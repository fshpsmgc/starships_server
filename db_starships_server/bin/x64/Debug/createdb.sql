CREATE TABLE hulls(
    id integer PRIMARY KEY,
    name TEXT NOT NULL,
    sprite_name TEXT NOT NULL,
    max_hp integer NOT NULL,
    weapon_mount_count integer NOT NULL,
    max_crew_count integer NOT NULL,
    max_fuel INTEGER NOT NULL,
    defence INTEGER NOT NULL,
    weight INTEGER NOT NULL 
);

CREATE TABLE engines (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    sprite_name TEXT NOT NULL,
    range INTEGER NOT NULL,
    speed INTEGER NOT NULL,
    weight INTEGER NOT NULL  
);

CREATE TABLE command_units (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    sprite_name TEXT NOT NULL,
    max_energy INTEGER NOT NULL,
    evasion INTEGER NOT NULL,
    weight INTEGER NOT NULL
);

CREATE TABLE weapons (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    sprite_name TEXT NOT NULL,
    damage INTEGER NOT NULL,
    accuracy INTEGER NOT NULL,
    fire_rate INTEGER NOT NULL
);

CREATE TABLE items (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    sprite_name TEXT NOT NULL,
    sell_cost INTEGER NOT NULL,
    description TEXT NOT NULL
);

CREATE TABLE players_system (
    id INTEGER PRIMARY KEY,
    public_name TEXT NOT NULL,
    login TEXT NOT NULL,
    password TEXT NOT NULL,
    salt TEXT NOT NULL,
    last_login_time INTEGER NOT NULL
);

CREATE TABLE missions (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    description TEXT NOT NULL,
    money_reward INTEGER NOT NULL,
    length INTEGER NOT NULL,
    distance INTEGER NOT NULL
);


CREATE TABLE ships (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,

    hull_id INTEGER,
    engine_id INTEGER,
    cu_id INTEGER,
    FOREIGN KEY(cu_id) REFERENCES command_units(id),
    FOREIGN KEY(engine_id) REFERENCES engines(id),
    FOREIGN KEY(hull_id) REFERENCES hulls(id)
);

CREATE TABLE players_logic (
    id INTEGER PRIMARY KEY,
    
    player_id INTEGER,
    ship_id INTEGER,
    currentMission INTEGER,
    fuel INTEGER NOT NULL,
    energy INTEGER NOT NULL,
    crew_count INTEGER NOT NULL,
    hp INTEGER NOT NULL,
    FOREIGN KEY(currentMission) REFERENCES missions(id),
    FOREIGN KEY(ship_id) REFERENCES ships(id),
    FOREIGN KEY(player_id) REFERENCES players_system(id)
);

CREATE TABLE players_weapons (
    id INTEGER PRIMARY KEY,
    player_id INTEGER,
    weapon_id INTEGER,
    FOREIGN KEY(weapon_id) REFERENCES weapons(id),
    FOREIGN KEY(player_id) REFERENCES hulls(id)
);

CREATE TABLE missions_reward (
    id INTEGER PRIMARY KEY,
    mission_id INTEGER,
    item_id INTEGER,
    FOREIGN KEY(item_id) REFERENCES items(id),
    FOREIGN KEY(mission_id) REFERENCES missions(id)
);

CREATE TABLE missions_enemies (
    id INTEGER PRIMARY KEY,
    mission_id INTEGER,
    ship_id INTEGER,
    FOREIGN KEY(ship_id) REFERENCES ships(id),
    FOREIGN KEY(mission_id) REFERENCES missions(id)
);

CREATE TABLE editors (
    id INTEGER PRIMARY KEY,
    user_id INTEGER,
    
    FOREIGN KEY(user_id) REFERENCES players_system(id)
);

CREATE TABLE inventory (
    id INTEGER PRIMARY KEY,
    player_id INTEGER,
    item_id INTEGER,
    
    FOREIGN KEY(item_id) REFERENCES items(id)
    FOREIGN KEY(player_id) REFERENCES players_system(id)
);