# Summary

Date : 2020-04-14 08:36:44

Directory d:\Dev\Games\starships_server\db_starships_server

Total : 25 files,  12713 codes, 111 comments, 171 blanks, all 12995 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| XML | 2 | 11,373 | 0 | 1 | 11,374 |
| C# | 8 | 747 | 111 | 141 | 999 |
| Database | 4 | 347 | 0 | 6 | 353 |
| JSON | 10 | 132 | 0 | 5 | 137 |
| SQL | 1 | 114 | 0 | 18 | 132 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 25 | 12,713 | 111 | 171 | 12,995 |
| Properties | 1 | 15 | 17 | 5 | 37 |
| bin | 16 | 11,855 | 0 | 30 | 11,885 |
| bin\Debug | 1 | 0 | 0 | 1 | 1 |
| bin\x64 | 14 | 11,855 | 0 | 28 | 11,883 |
| bin\x64\Debug | 14 | 11,855 | 0 | 28 | 11,883 |
| bin\x64\Debug\examples | 9 | 92 | 0 | 5 | 97 |
| bin\x86 | 1 | 0 | 0 | 1 | 1 |
| bin\x86\Debug | 1 | 0 | 0 | 1 | 1 |

[details](details.md)