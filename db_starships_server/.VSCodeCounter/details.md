# Details

Date : 2020-04-14 08:36:44

Directory d:\Dev\Games\starships_server\db_starships_server

Total : 25 files,  12713 codes, 111 comments, 171 blanks, all 12995 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [CommandProcessor.cs](/CommandProcessor.cs) | C# | 160 | 36 | 33 | 229 |
| [DBController.cs](/DBController.cs) | C# | 171 | 47 | 30 | 248 |
| [DataClasses.cs](/DataClasses.cs) | C# | 60 | 0 | 9 | 69 |
| [Log.cs](/Log.cs) | C# | 98 | 2 | 20 | 120 |
| [MessageProcessor.cs](/MessageProcessor.cs) | C# | 153 | 1 | 27 | 181 |
| [Program.cs](/Program.cs) | C# | 20 | 0 | 3 | 23 |
| [Properties/AssemblyInfo.cs](/Properties/AssemblyInfo.cs) | C# | 15 | 17 | 5 | 37 |
| [Server.cs](/Server.cs) | C# | 70 | 8 | 14 | 92 |
| [bin/Debug/starships.db](/bin/Debug/starships.db) | Database | 0 | 0 | 1 | 1 |
| [bin/x64/Debug/Newtonsoft.Json.xml](/bin/x64/Debug/Newtonsoft.Json.xml) | XML | 11,262 | 0 | 1 | 11,263 |
| [bin/x64/Debug/createdb.sql](/bin/x64/Debug/createdb.sql) | SQL | 114 | 0 | 18 | 132 |
| [bin/x64/Debug/examples/command_units_example.json](/bin/x64/Debug/examples/command_units_example.json) | JSON | 14 | 0 | 1 | 15 |
| [bin/x64/Debug/examples/engines_example.json](/bin/x64/Debug/examples/engines_example.json) | JSON | 14 | 0 | 0 | 14 |
| [bin/x64/Debug/examples/hulls_example.json](/bin/x64/Debug/examples/hulls_example.json) | JSON | 20 | 0 | 0 | 20 |
| [bin/x64/Debug/examples/items_example.json](/bin/x64/Debug/examples/items_example.json) | JSON | 12 | 0 | 0 | 12 |
| [bin/x64/Debug/examples/missions_enemies_example.json](/bin/x64/Debug/examples/missions_enemies_example.json) | JSON | 4 | 0 | 2 | 6 |
| [bin/x64/Debug/examples/missions_example.json](/bin/x64/Debug/examples/missions_example.json) | JSON | 14 | 0 | 0 | 14 |
| [bin/x64/Debug/examples/missions_rewards_example.json](/bin/x64/Debug/examples/missions_rewards_example.json) | JSON | 0 | 0 | 1 | 1 |
| [bin/x64/Debug/examples/ships_example.json](/bin/x64/Debug/examples/ships_example.json) | JSON | 0 | 0 | 1 | 1 |
| [bin/x64/Debug/examples/weapons_example.json](/bin/x64/Debug/examples/weapons_example.json) | JSON | 14 | 0 | 0 | 14 |
| [bin/x64/Debug/hulls.json](/bin/x64/Debug/hulls.json) | JSON | 40 | 0 | 0 | 40 |
| [bin/x64/Debug/log.db](/bin/x64/Debug/log.db) | Database | 223 | 0 | 0 | 223 |
| [bin/x64/Debug/starships.db](/bin/x64/Debug/starships.db) | Database | 124 | 0 | 4 | 128 |
| [bin/x86/Debug/starships.db](/bin/x86/Debug/starships.db) | Database | 0 | 0 | 1 | 1 |
| [db_starships_server.csproj](/db_starships_server.csproj) | XML | 111 | 0 | 0 | 111 |

[summary](results.md)