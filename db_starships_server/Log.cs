﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;

namespace db_starships_server {
    class Log {
        public static string dbLogFilename = "log.db";

        public static void Init() {
            if (File.Exists(dbLogFilename))
                return;

            string dbInitCommand = "CREATE TABLE log(" +
                                        "id INTEGER PRIMARY KEY, " +
                                        "timestamp TEXT NOT NULL," +
                                        "type TEXT NOT NULL," +
                                        "message TEXT NOT NULL)";
            SQLiteConnection.CreateFile(dbLogFilename);

            using (SQLiteConnection connection = new SQLiteConnection($"Data Source={dbLogFilename};Version=3;")) {
                connection.Open();
                SQLiteCommand command = new SQLiteCommand(dbInitCommand, connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public static void PrintTable(SQLiteDataReader data) {
             do {
                if (data.FieldCount == 0) {
                    Error("Command output cannot be shown");
                    return;
                }

                Console.WriteLine(data.GetTableName(0));

                for (int i = 0; i < data.FieldCount; i++) {
                    Console.Write($"{{0, {data.GetName(i).Length}}}", data.GetName(i) + "|");
                }

                Console.WriteLine();

                while (data.Read()) {
                    for (int i = 0; i < data.FieldCount; i++) {
                        switch (data.GetDataTypeName(i).ToLower()) {
                            case "integer":
                                Console.Write($"{{0, {data.GetName(i).Length}}}", data.GetInt32(i) + "|");
                                break;
                            case "text":
                                Console.Write($"{{0, {data.GetName(i).Length}}}", data.GetString(i) + "|");
                                break;
                            default:
                                Console.Write($"{{0, {data.GetName(i).Length}}}", data.GetDataTypeName(i) + "|");
                                break;
                        }
                    }
                    Console.WriteLine();
                }

                Console.WriteLine();
            } while (data.NextResult());
        }

        public static void Write(object o, string src = "console") {
            if(src == "console")
                Console.WriteLine(o);

            string log = o.ToString();
            log = log.Replace("\r", "");
            log = log.Replace("\n", "");

            if(src != "console")
            writeToDB(new QueryParameter[] { new QueryParameter("@timestamp",  DateTime.Now.ToString("F")),
                                             new QueryParameter("@source", src),
                                             new QueryParameter("@log", log)});


            //DBController.ExecuteNonQuery($"INSERT INTO log(timestamp, type, message) VALUES ({DateTime.Now.ToString("F")}, {src}, {log})");
            //File.AppendAllText(dbLogFilename, $"{DateTime.Now.ToString("F")}\t {src}\t {log}\n");
        }

        private static void writeToDB(QueryParameter[] queryParameters) {
            string query = "INSERT INTO log(timestamp, type, message) VALUES (@timestamp, @source, @log)";
            using (SQLiteConnection connection = Connect()) {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand(query, connection)) {
                    foreach (var v in queryParameters) {
                        command.Parameters.AddWithValue(v.parameter, v.value);
                    }
                    command.Prepare();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        public static SQLiteConnection Connect() {
            return new SQLiteConnection($"Data Source={dbLogFilename};Version=3;");
        }

        public static void Warning(object o) {
            ConsoleColor col = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(o);
            Console.ForegroundColor = col;
        }
        
        public static void Error(object o) {
            ConsoleColor col = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(o);
            Console.ForegroundColor = col;
        }
    }
}
