﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Security.Cryptography;
using System.IO;


namespace db_starships_server {
    struct QueryParameter {
        public string parameter;
        public object value;

        public QueryParameter(string p, object v) {
            parameter = p;
            value = v;
        }
    }

    class DBController {
        public static string dbFilename = "starships.db";
        public static string dbInitFilename = "createdb.sql";
        
        static RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
        
        /// <summary>
        /// Create database
        /// </summary>
        public static void CreateDatabase() {
            Log.Write("Creating database");
            if (!File.Exists(dbInitFilename)) {
                Console.WriteLine($"Couldn't find {dbInitFilename}");
                Console.ReadLine();
                Environment.Exit(1);
            }

            //Log.Create();

            string dbInitCommand = File.ReadAllText(dbInitFilename);
            SQLiteConnection.CreateFile(dbFilename);
            ExecuteNonQuery(dbInitCommand, false);

            Log.Write("Finished creating database");
        }

        /// <summary>
        /// Recreate database from scratch
        /// </summary>
        public static void ResetDatabase() {
            if (!File.Exists(dbInitFilename)) {
                Log.Error($"Cannot find SQL file to recreate initial state of database at {Path.Combine(Directory.GetCurrentDirectory(), dbInitFilename)}");
                return;
            }

            string confirmationString = GetSalt(10);
            Log.Warning($"You are about to reset database. This will erase all information including user data. Write {confirmationString} to confirm");
            string input = Console.ReadLine();
            if (input == confirmationString) {
                if (File.Exists(dbFilename)) {
                    try{
                        File.Delete(dbFilename);
                    }catch(IOException e) {
                        Log.Error(e);
                        return;
                    }
                }

                CreateDatabase();
            }
        }

        public static void CreateBackup(string msg = "") {
            if (File.Exists(dbFilename)) {
                string backupFilename = $"{dbFilename}-{DateTime.Now.ToString("dd-mm-yyyy-hh-mm-ss")}{msg}.backup";
                File.Copy(dbFilename, backupFilename);

                Console.WriteLine($"Backup {backupFilename} is created");
            }
        }

        public static void Restore() {
            Log.Write("Restoring database");
            CreateBackup("-restore");

            var files = Directory.GetFiles(".", "*.backup");

            Console.WriteLine("Select backup");
            for (int i = 0; i < files.Length; i++) {
                Console.WriteLine($"{i}. {files[i]}");
            }

            int selectedBackup = 0;
            do {
                try {
                    selectedBackup = Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception e) {
                    Console.WriteLine(e);
                }
            } while (selectedBackup >= files.Length || selectedBackup < 0);

            File.Delete(dbFilename);
            File.Move(files[selectedBackup], dbFilename);

            Console.WriteLine($"Restored database from {files[selectedBackup]}");
        }

        /// <summary>
        /// Get crypto salt
        /// </summary>
        /// <param name="maxSaltLength"></param>
        /// <returns></returns>
        public static string GetSalt(int maxSaltLength = 32) {
            var salt = new byte[maxSaltLength];

            rng.GetNonZeroBytes(salt);

            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < salt.Length; i++) {
                builder.Append(salt[i].ToString("x2"));
            }
            return builder.ToString();
        }

        /// <summary>
        /// Return data hash
        /// </summary>
        /// <param name="rawData"></param>
        /// <returns></returns>
        public static string ComputeHash(string rawData) {
            using (SHA256 sha256 = SHA256.Create()) {
                byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(rawData));
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++) {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        /// <summary>
        /// Establish connection to database
        /// </summary>
        /// <returns></returns>
        public static SQLiteConnection ConnectToDatabase() {
            return new SQLiteConnection($"Data Source={dbFilename};Version=3;");
        }

        /// <summary>
        /// Check if database exists
        /// </summary>
        /// <returns></returns>
        public static bool Exists() {
            return File.Exists(dbFilename);
        }

        /// <summary>
        /// Execute SQL query that doesn't return values
        /// </summary>
        /// <param name="query"></param>
        public static void ExecuteNonQuery(string query, bool record = true) {
            using (SQLiteConnection connection = ConnectToDatabase()) {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand(query, connection)) {
                    try {
                        command.ExecuteNonQuery();
                    }
                    catch (SQLiteException e) {
                        Log.Error(e.Message);
                        connection.Close();
                        return;
                    }

                    if (record)
                        Log.Write(query, "sql");
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Execute prepared SQL query that doesn't return values
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        public static void ExecuteNonQuery(string query, QueryParameter[] parameters, bool record = true) {
            using (SQLiteConnection connection = ConnectToDatabase()) {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand(query, connection)) {
                    foreach (var v in parameters) {
                        command.Parameters.AddWithValue(v.parameter, v.value);
                    }
                    command.Prepare();
                    try {
                        command.ExecuteNonQuery();
                    }
                    catch (SQLiteException e) {
                        Log.Error(e.Message);
                        connection.Close();
                        return;
                    }
                    if (record)
                        Log.Write(query, "sqlp");
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Execute SQL query and return DataReader
        /// </summary>
        /// <param name="query"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public static SQLiteDataReader ExecuteQuery(string query, SQLiteConnection connection, bool record = true) {
            connection.Open();
            SQLiteCommand command = new SQLiteCommand(query, connection);
            SQLiteDataReader data = null;
            try {
                data = command.ExecuteReader();
            }catch(SQLiteException e) {
                Log.Error(e.Message);
                return null;
            }

            if(record) Log.Write(query, "sqlq");
            return data;
        }

        /// <summary>
        /// Execute prepared SQL query and return DataReader
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public static SQLiteDataReader ExecuteQuery(string query, QueryParameter[] parameters, SQLiteConnection connection, bool record = true) {
            SQLiteDataReader data = null;
            connection.Open();
            SQLiteCommand command = new SQLiteCommand(query, connection);
            foreach (var v in parameters) {
                command.Parameters.AddWithValue(v.parameter, v.value);
            }
            command.Prepare();
            try {
                data = command.ExecuteReader();
            }
            catch(SQLiteException e) {
                Log.Error(e.Message);
                return null;
            }
            if(record) Log.Write(query, "sqlqp");
            return data;
        }
    }
}
