﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using Newtonsoft.Json;
using System.IO;

namespace db_starships_server {
    class CommandProcessor {
        static bool initialized = false;
        
        public static Dictionary<string, Action<string>> loadCommands = new Dictionary<string, Action<string>>();
        public static Dictionary<string, Tuple<string, Action<string[]>>> mainCommands = new Dictionary<string, Tuple<string, Action<string[]>>>();

        public static void Init() {
            InitMainCommands();
            InitLoadCommands();

            initialized = true;
        }

        public static void InitMainCommands() {
            mainCommands.Add("sql",  new Tuple<string, Action<string[]>>("executes sql command", Sql));
            mainCommands.Add("sqlq", new Tuple<string, Action<string[]>>("executes sql commands and prints result", Sqlq));
            mainCommands.Add("exit", new Tuple<string, Action<string[]>>("shuts down the server", Exit));
            mainCommands.Add("load", new Tuple<string, Action<string[]>>("loads JSON file of a specified type", Load));
            mainCommands.Add("help", new Tuple<string, Action<string[]>>("shows this message", ShowHelp));
            mainCommands.Add("db",   new Tuple<string, Action<string[]>>("handles database actions", DatabaseHandling));
        }

        public static void InitLoadCommands() {
            loadCommands.Add("hull", LoadJSON<hulls>);
            loadCommands.Add("command", LoadJSON<command_units>);
            loadCommands.Add("engine", LoadJSON<engines>);
            loadCommands.Add("item", LoadJSON<items>);
            loadCommands.Add("weapon", LoadJSON<weapons>);
            loadCommands.Add("ship", LoadJSON<ships>);
            loadCommands.Add("mission_enemy", LoadJSON<missions_enemies>);
            loadCommands.Add("mission_reward", LoadJSON<missions_rewards>);
            loadCommands.Add("mission", LoadJSON<missions>);
        }

        /// <summary>
        /// Choose an appropriate function for passed command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="server"></param>
        public static void ProcessCommand(string command, Server server) {
            Console.WriteLine($"Processing command {command}");

            string[] args = command.Split(' ');

            Tuple<string, Action<string[]>> tuple;
            if (mainCommands.TryGetValue(args[0], out tuple)) {
                tuple.Item2(args);
            } else {
                Log.Error($"Invalid command type: \"{args[0]}\"");
            }
        }

        /// <summary>
        /// Execute SQL query without feedback
        /// </summary>
        /// <param name="command"></param>
        private static void Sql(string[] args) {
            string query = string.Empty;
            for (int i = 1; i < args.Length - 1; i++) {
                query += args[i] + ' ';
            }
            query += args[args.Length - 1];

            DBController.ExecuteNonQuery(query);
            Log.Write("Command executed");
        }

        /// <summary>
        /// Execute SQL query and print feedback
        /// </summary>
        /// <param name="command"></param>
        private static void Sqlq(string[] args) {
            string query = string.Empty;
            for (int i = 1; i < args.Length - 1; i++) {
                query += args[i] + ' ';
            }
            query += args[args.Length - 1];

            using (SQLiteConnection connection = DBController.ConnectToDatabase()) {
                SQLiteDataReader data = DBController.ExecuteQuery(query, connection);
                if (data != null) {
                    Log.PrintTable(data);
                } else {
                    Log.Error("Could not retrieve data");
                }
                connection.Close();
                Log.Write("Command executed");
            }
        }

        /// <summary>
        /// Load database entity from file
        /// </summary>
        /// <param name="command"></param>
        private static void Load(string[] args) {
            string fileType = args[1];
            string fileName = args[2];

            Log.Write($"Loading file {fileName} of type {fileType}");

            Action<string> action;
            if (loadCommands.TryGetValue(fileType, out action)) {
                action(fileName);
            }
            else{
                Log.Error($"Invalid data type: \"{fileType}\"");
            }
        }

        /// <summary>
        /// Convert JSON file to data class and insert it into database
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName"></param>
        private static void LoadJSON<T>(string fileName) {
            if (!File.Exists(fileName)) {
                Log.Error($"Couldn't load file {Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + fileName}");
                return;
            }

            string json = File.ReadAllText(fileName);

            List<T> obj = new List<T>();
            JsonReader reader = new JsonTextReader(new StringReader(json));
            reader.SupportMultipleContent = true;

            while (true) {
                if (!reader.Read()) {
                    break;
                }

                JsonSerializer serializer = new JsonSerializer();
                T o = serializer.Deserialize<T>(reader);

                obj.Add(o);
            }

            InsertInDB<T>(obj);
        }

        /// <summary>
        /// Insert objects into database
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        private static void InsertInDB<T>(List<T> obj) {
            foreach (var v in obj) {
                InsertInDB<T>(v);
            }
        }

        /// <summary>
        /// Insert object into database
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        private static void InsertInDB<T>(T obj) {
            string sql = $"INSERT INTO {obj.GetType().Name}(";
            var fields = obj.GetType().GetFields();
            for (int i = 0; i < fields.Length; i++) {
                sql += fields[i].Name;
                if (i != fields.Length - 1) {
                    sql += ",";
                }
            }

            sql += ") VALUES (";

            for (int i = 0; i < fields.Length; i++) {
                if (fields[i].GetValue(obj).GetType() == typeof(string)) {
                    sql += $"\"{fields[i].GetValue(obj).ToString()}\"";
                } else {
                    sql += fields[i].GetValue(obj).ToString();
                }
                if (i != fields.Length - 1) {
                    sql += ",";
                }
            }

            sql += ")";

            DBController.ExecuteNonQuery(sql);
        }

        /// <summary>
        /// Show help command describing all commands
        /// </summary>
        /// <param name="args"></param>
        private static void ShowHelp(string[] args) {
            foreach (var v in mainCommands) {
                Console.WriteLine($"{v.Key}: {v.Value.Item1}");
            }
            Console.WriteLine();
        }

        private static void DatabaseHandling(string[] args) {
            switch (args[1]) {
                case "reset":
                    DBController.ResetDatabase();
                    break;
                case "backup":
                    if (args.Length > 2) {
                        string msg = "";
                        for (int i = 2; i < args.Length; i++) {
                            msg += args[i];
                        }
                        DBController.CreateBackup(msg);
                    } else {
                        DBController.CreateBackup();
                    }
                    break;
                case "restore":
                    DBController.Restore();
                    break;
                default:
                    Log.Error("Invalid argument");
                    break;
            }
        }

        private static void Exit(string[] args) {
            Log.Write("Server stopped", "system");
            Environment.Exit(0);
        }
    }
}
