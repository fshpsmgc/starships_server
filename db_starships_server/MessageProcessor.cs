﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace db_starships_server {
    class MessageProcessor {
        public static void ProcessMessage(string input, Server server) {
            foreach (var msg in input.Split('\n')) {
                if (msg == string.Empty)
                    continue;
                Console.WriteLine($"Message {msg} recieved from client");

                string msgType = msg.Split(':')[0];

                switch (msgType) {
                    case "login":
                        ProcessLoginRequest(msg, server);
                        break;
                    case "register":
                        ProcessRegisterRequest(msg, server);
                        break;
                    case "req":
                        ProcessResourceRequest(msg, server);
                        break;
                    case "sell":
                        ProcessItemSale(msg, server);
                        break;
                    default:
                        Log.Error($"Invalid message recieved: {msg}");
                        break;
                }
            }
        }

        private static void ProcessResourceRequest(string msg, Server server) {
            string type = msg.Split(':')[1];
            switch (type) {
                case "engines":
                    SendEngineList(server);
                    break;
                case "hulls":
                    SendHullsList(server);
                    break;
                case "cu":
                    SendCUList(server);
                    break;
                case "quests":
                    SendQuestList(server);
                    break;
                case "inventory":
                    SendInventoryInformation(server, Convert.ToInt32(msg.Split(':')[2]));
                    break;
                default:
                    Log.Error($"Invalid resource type requested: {type}");
                    break;
            }
        }

        //TODO
        private static void ProcessItemSale(string msg, Server server) {
            //SQLiteConnection connection = DBController.ConnectToDatabase();
            //SQLiteDataReader data = DBController.ExecuteQuery("")
        }

        private static void SendEngineList(Server server) {
            SQLiteConnection connection = DBController.ConnectToDatabase();
            SQLiteDataReader data = DBController.ExecuteQuery("SELECT * FROM engines", connection);
            string msg = "engines_data\n";

            while (data.Read()) {
                msg += $"{data.GetString(1)},{data.GetString(2)},{data.GetInt32(3)},{data.GetInt32(4)},{data.GetInt32(5)}\n";
            }
            connection.Close();
            server.SendMessage(msg);
        }

        private static void SendHullsList(Server server) {
            SQLiteConnection connection = DBController.ConnectToDatabase();
            SQLiteDataReader data = DBController.ExecuteQuery("SELECT * FROM hulls", connection);
            string msg = "hulls_data\n";

            while (data.Read()) {
                msg += $"{data.GetString(1)},{data.GetString(2)},{data.GetInt32(3)},{data.GetInt32(4)},{data.GetInt32(5)}\n";
            }
            connection.Close();
            server.SendMessage(msg);
        }

        private static void SendCUList(Server server) {
            SQLiteConnection connection = DBController.ConnectToDatabase();
            SQLiteDataReader data = DBController.ExecuteQuery("SELECT * FROM command_units", connection);
            string msg = "cu_data\n";

            while (data.Read()) {
                msg += $"{data.GetString(1)},{data.GetString(2)},{data.GetInt32(3)},{data.GetInt32(4)},{data.GetInt32(5)}\n";
            }
            connection.Close();
            server.SendMessage(msg);
        }

        private static void SendQuestList(Server server) {
            SQLiteConnection connection = DBController.ConnectToDatabase();
            SQLiteDataReader data = DBController.ExecuteQuery("SELECT * FROM missions", connection);
            string msg = "quests:\n";

            while (data.Read()) {
                msg += $"{data.GetString(1)},{data.GetString(2)},{data.GetInt32(3)},{data.GetInt32(4)},{data.GetInt32(5)}\n";
            }
            connection.Close();
            server.SendMessage(msg);
        }

        private static void SendInventoryInformation(Server server, int playerID) {
            SQLiteConnection connection = DBController.ConnectToDatabase();
            string query = "select items.* from items inner join inventory on items.id = inventory.item_id where inventory.player_id = @playerID";
            QueryParameter[] parameters = new QueryParameter[1];
            parameters[0] = new QueryParameter("@playerID", playerID);
            SQLiteDataReader data = DBController.ExecuteQuery(query, parameters, connection);
            string msg = "inventory:\n";
            
            while (data.Read()) {
                msg += $"{data.GetInt32(0)},{data.GetString(1)},{data.GetString(2)},{data.GetInt32(3)},{data.GetString(4)}\n";
            }
            connection.Close();
            server.SendMessage(msg);
        }

        private static void ProcessLoginRequest(string msg, Server server) {
            string[] tokens = msg.Substring("login:".Length).Split(',');
            string username = tokens[0];
            string password = tokens[1];

            string query = $"SELECT id, login, password, salt FROM players_system WHERE login=@username";

            QueryParameter[] parameters = new QueryParameter[1];
            parameters[0] = new QueryParameter("@username", username);

            SQLiteConnection connection = DBController.ConnectToDatabase();
            SQLiteDataReader data = DBController.ExecuteQuery(query, parameters, connection);

            if (!data.HasRows) {
                server.SendMessage("incorrect username");
            } else {
                while (data.Read()) {
                    if (DBController.ComputeHash(password + data.GetString(3)) == data.GetString(2)) {
                        server.SendMessage($"login ok:{data.GetInt32(0)}");
                        DBController.ExecuteNonQuery($"UPDATE players_system SET last_login_time = \"{DateTime.Now.ToString()}\" WHERE id = {data.GetInt32(0)}");

                    } else {
                        server.SendMessage("incorrect password");
                    }
                }
            }

            connection.Close();
        }

        private static void ProcessRegisterRequest(string msg, Server server) {
            string[] tokens = msg.Substring("register:".Length).Split(',');
            string username = tokens[0];
            string password = tokens[1];
            string publicName = tokens[2];
            string lastLogin = DateTime.Now.ToString();
            string salt = DBController.GetSalt();

            string query = $"INSERT INTO players_system (login, password, salt, public_name, last_login_time) VALUES (@username, @password, @salt, @name, @lastLogin); SELECT last_insert_rowid()";

            QueryParameter[] parameters = new QueryParameter[5];
            parameters[0] = new QueryParameter("@username", username);
            parameters[1] = new QueryParameter("@password", DBController.ComputeHash(password + salt));
            parameters[2] = new QueryParameter("@salt", salt);
            parameters[3] = new QueryParameter("@name", publicName);
            parameters[4] = new QueryParameter("@lastLogin", lastLogin);

            SQLiteConnection connection = DBController.ConnectToDatabase();
            SQLiteDataReader data = DBController.ExecuteQuery(query, parameters, connection);
            while (data.Read()) {
                server.SendMessage($"login ok:{data.GetInt32(0)}");
                return;
            }
            
            connection.Close();
            //TODO: Send "login failed" just in case
        }
    }
}
