﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace db_starships_server {
	public class Server{

		private TcpListener tcpListener;
		private TcpClient connectedTcpClient;

		private Thread tcpListenerThread;

		private int Port;
		private Thread inputThread;

		// Use this for initialization
		public Server(int port) {
			Port = port;
			// Start TcpServer background thread 		
			tcpListenerThread = new Thread(new ThreadStart(ListenForIncommingRequests));
			tcpListenerThread.IsBackground = true;
			tcpListenerThread.Start();

			inputThread = new Thread(new ThreadStart(ListenInput));
			inputThread.Start();
		}

		private void ListenInput() {
			while (true) {
				string command = Console.ReadLine();
				CommandProcessor.ProcessCommand(command, this);
			}
		}

		/// <summary> 	
		/// Runs in background TcpServerThread; Handles incomming TcpClient requests 	
		/// </summary> 	
		private void ListenForIncommingRequests() {
			try {	
				tcpListener = new TcpListener(IPAddress.Any, Port);
				tcpListener.Start();

				Console.WriteLine($"Server is listening on port {Port}");
				Byte[] bytes = new Byte[1024];
				
				while (true) {
					using (connectedTcpClient = tcpListener.AcceptTcpClient()) {
						using (NetworkStream stream = connectedTcpClient.GetStream()) {
							int length;			
							while ((length = stream.Read(bytes, 0, bytes.Length)) != 0) {
								var incommingData = new byte[length];
								Array.Copy(bytes, 0, incommingData, 0, length);
								string clientMessage = Encoding.ASCII.GetString(incommingData);
								MessageProcessor.ProcessMessage(clientMessage, this);
							}
						}
					}
				}
			}
			catch (SocketException socketException) {
				Console.WriteLine("SocketException " + socketException.ToString());
			}
		}


		/// <summary> 	
		/// Send message to client using socket connection. 	
		/// </summary> 	
		public void SendMessage(string message) {
			if (connectedTcpClient == null) {
				return;
			}

			try {
				NetworkStream stream = connectedTcpClient.GetStream();
				if (stream.CanWrite) {
					byte[] serverMessageAsByteArray = Encoding.ASCII.GetBytes(message);
					stream.Write(serverMessageAsByteArray, 0, serverMessageAsByteArray.Length);
					Console.WriteLine($"Message \"{message}\" sent!");
				}
			}
			catch (SocketException socketException) {
				Console.WriteLine("Socket exception: " + socketException);
			}
		}
	}
}
